---
layout: single
title: "FLOSS Foundations at FOSDEM 2006"
date: 2006-02-01
permalink: /flossfoundations/node/10
categories:
    - meetings
---

The Free/Libre Open Source Software Project Summit aims to be a place for the people who try to Get Stuff Done at the various non-profits around open source to congregate, share information, and pick each others' brains. This will be the third FLOSS summit, following in the vein of the original summit.

## When?

Friday, February 24, 2006, 13:00 - 18:00 in Brussels. The summit will be held just before [FOSDEM](https://fosdem.org).

## Where?

The meeting location is: Novotel Brussels Centre Tour-Noire. This is very close to, but not the same as the speakers hotel for FOSDEM 2006 (which is Ibis Brussels Centre Sainte Catherine). You'll find it in the centre of Brussels, at Rue de la Vierge Noire 32. Look at this [map](http://web.archive.org/web/20060816023124/http://www.nlnet.nl/~wytze/floss/Novotel-map.jpg) for directions to get there.

Inside the hotel, look for the meeting rooms. Our meeting room should be labelled as FLOSSFOSDEM / NLNET. If you have trouble finding the room, try calling Wytze on his mobile: +31 6 44030889.

There is WiFi available (presumably at hotel prices), and one ADSL internet connection in the room has been promised. If somebody wants to bring a little router to hook on, please let us know!

A coffee/tea/pastry break has been scheduled with the hotel at 15:30. Soft drinks will be available in the meeting room.

## Who?

Present:

* Gervase Markham (gerv@mozilla.org), Mozilla Foundation
* Wytze van der Raay (wytze@nlnet.nl), Stichting NLnet
* Cornelius Schumacher (schumacher@kde.org), KDE
* Wim Vandeputte (wvdputte@openbsd.org), OpenBSD & OpenSSH

Space is limited to 13 people. The initial invitation list is:

* foundations@lists.freedesktop.org

Please suggest others to invite:

Regrets:

* David Neary, GNOME Foundation
* Peter Saint-Andre, Jabber Software Foundation

## Agenda

Please add your agenda points below!

## Suggested agenda points

* What do you think of [GPLv3](http://gplv3.fsf.org/)? Does your group have issues with it?

## Write-Up

by Gervase Markham

There were between two and four attendees throughout the day. In order of arrival, they were:

* Wytze van der Raay (Stichting NLnet)
* Gervase Markham (Mozilla Foundation)
* Anne Ostergaard (Vice President, GNOME Foundation)
* Wim Vandeputte (OpenBSD and OpenSSH) - there for an hour or so

Although other topics were discussed, like GPLv3, from a Mozilla Foundation point of view the most important discussion was one about criteria etc. for grantmaking, where Wytze explained how Stichting NLnet did it.

He said they had three criteria:

1. Must be to do with "Internet technology"
1. Any software results must be free software (GPL preferred)
1. It must be useful - a promotion plan must be submitted

The following points were made:

* They make early decisions; they don't require a written proposal to consider an idea
* They do a small number of larger projects, but have never managed to spend their entire budget (lack of proposals, not poor quality)
* They spent €1M or so last year; exact numbers on the web
* They list projects and amounts donated in the public annual report; transparency is very important to them
* Payments are usually staged
* Their board meets every 6-8 weeks, it's five people, and they approve all grants (but are considering expert advisory committees for one or more technical areas)

We had a discussion of a problem Stichting NLnet do not have, but the MoFo might - how to balance involvement with breadth? Doing a lot of projects means you can devote less management time to each one.

Wim said that many students in Europe write theses or do projects, but these need "legitimacy" - that is, at least in Germany, they must be working for a business.

It was considered that perhaps a three-way partnership with businesses (or universities) would work:

* The Foundation and the business both pay the student 50%
* The business provides management input, mentoring and "legitimacy"
* The Foundation's money reduces the cost to the business
* Both the business and the Foundation get software they want

This might allow the Foundation to find more projects by offloading the management and mentoring effort.

We also briefly discussed how to avoid grant-funded work endangering or disrupting community efforts. A waiting period where proposed grants are published for comment was suggested.
