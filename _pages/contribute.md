---
title: Contribute to FLOSS Foundations
layout: single
permalink: /contribute/
---

## Code of Conduct

The FLOSS Foundations community, including all engagement with this repository, operates according to a [Code of Conduct](https://www.freedesktop.org/wiki/CodeOfConduct).

## Update the Foundations Directory

We're very aware that the Foundations Directory is missing a lot of entries and some that it does have could use some updating.

Now that the site is all [Markdown](https://en.wikipedia.org/wiki/Markdown), it's our hope that the community will be able to help keep the directory more up-to-date.

There are two ways to let us know about updates for the directory:

1. Send a [merge request](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/#code-review-with-gitlab) (MR) with the necessary changes. This is the quickest way to update the directory. The site maintainers will review your MR and either ask for changes (if needed) or merge it (most likely).
1. Open [an issue]( {{ site.new_issue }} ) with the changes that you're suggesting. This is a great way to contribute if you don't know Markdown or simply don't have the time to send a full merge request.

If you ever have any questions about updating the directory, please open [an issue]( {{ site.new_issue }} ) with your question or concern. We'll have a look just as soon as we can.

## Help with the website

All changes to the website should first be logged in [an issue]( {{ site.new_issue }} ). This gives us the opportunity to discuss the change before you invest all the time in making the change itself.

After that, feel free to send over a [merge request](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/#code-review-with-gitlab) (MR) with the necessary changes.

## Other ways to contribute

1. Join the [mailing list](http://lists.freedesktop.org/mailman/listinfo/foundations)! Sharing your knowledge with the rest of the FLOSS Foundations community is a valuable way to contribute.
1. Report website bugs or any other concerns in the [issue tracker]( {{ site.issues }} ).
