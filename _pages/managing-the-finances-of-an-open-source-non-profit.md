---
layout: single
title: "Managing the Finances of an Open Source Non-Profit"
permalink: /node/8
---

* Overview of information and stats on the Apache Software Foundation (see http://www.apache.org/foundation/)
* An overview of the requirements for a foundation
* Direct contributions via Paypal
* One reasonable source of funds was http://donateacar.com/
* ~ $50k from Google
* ~ $25k from conferences
* A quick overview of the history of finances at the ASF
* A quick overview of the requirements for the various forms of US non-profits
* Justin mentions that the ASF received excellent help from the Software Freedom Legal Center (http://softwarefreedom.org) - pro-bono, but only for FLOSS projects

Tools

* SVN is used to keep a consistent log of activities
* Quickbooks
