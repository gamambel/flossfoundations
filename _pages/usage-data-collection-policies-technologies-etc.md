---
layout: single
title: "Usage Data Collection- Policies, Technologies, etc."
permalink: /usage-data-collection-policies-technologies-etc
---

Some notes on groups that do data collection in order to have a better idea of the size/character of their users.

## Mozilla

https://wiki.mozilla.org/Browser_Metrics

http://www.mozilla.com/en-US/legal/privacy/firefox-en.html (in particular, 'automated update services')
opt-out; all data is gathered purely from information in http headers in updates, so no extra tech built in.

## Wordpress

http://ma.tt/2008/07/26-by-the-numbers/ (look for 'update system'; can't find other details)

## CiviCRM

http://civicrm.org/node/274 (current system)

http://civicrm.org/node/430 (proposed collection for next release)

no privacy policy/data retention policy yet, but do md5 the URL of data submitters. Do not discard IP numbers from that log file; feel they probably should.

found it useful; twice as many Joomla-based users as expected

opt-out.

## Eclipse

data heavy- more to get a sense of what features people are using, rather than how many users there are. opt-in as a result of privacy concerns; over 30K opt-in users.

Web site for UDC: http://www.eclipse.org/epp/usagedata/index.php

Terms of use: http://www.eclipse.org/org/usagedata/terms.php

Two communications regarding UDC:

http://dev.eclipse.org/blogs/mike/2008/06/05/collecting-usage-data/

http://dev.eclipse.org/blogs/mike/2008/06/06/using-usage-data/
